import { fromEvent, combineLatest } from 'rxjs';
import { map, debounceTime } from 'rxjs/operators';
import { calculateMortgage } from './calculate';

const getAccessToDOM = (id: string) =>
  <HTMLInputElement>document.getElementById(id);

const loanAmount = getAccessToDOM(`loanAmount`);
const loanInterest = getAccessToDOM(`loanInterest`);
const loanLength = getAccessToDOM(`loanLength`);
const display = document.getElementById(`result`);

const getValue = (value: any, pos: string) =>
  fromEvent(value, pos).pipe(
    map(el => el.target.value),
    debounceTime(2000)
  );

const loanInterest$ = getValue(loanInterest, `input`);
const loanAmount$ = getValue(loanAmount, `input`);
const loanLength$ = getValue(loanLength, `input`);

const result = combineLatest([loanInterest$, loanAmount$, loanLength$]).pipe(
  map(([a, b, c]) => calculateMortgage(a, b, c).toString())
);

result.subscribe(res => (display.innerHTML = `<h2>${res}</h2>`));